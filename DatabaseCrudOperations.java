// this java class is used to perform corresponding CURD operations 


import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DatabaseCrudOperations {
    public static int createTable(String ctname,int colcount,String pkey ,Statement st,String query) throws SQLException
    {
        int i=0;
    try
    {
    i =  st.executeUpdate(query);
    if(i==1)
             System.out.println("table created in create table method");
         else
         {
             
             System.out.println("table not created ");
         }
    }
    catch(Exception e)
    {
        System.out.println(e);
        
        System.out.println("table already exists or wrong syntax of query");
            return -1;
        
    }
    return i;
    }
    public static int retrieveTable(String rtname,int pkey,String query,Statement st)
    {
    int flag=0;
        try {
            ResultSet rs=st.executeQuery(query);
            ResultSetMetaData rsmd=rs.getMetaData();
            int colc = rsmd.getColumnCount();
            System.out.println("Total columns: "+rsmd.getColumnCount());
            System.out.println("Column Name of 1st column: "+rsmd.getColumnName(1));
            System.out.println("Column Type Name of 1st column: "+rsmd.getColumnTypeName(1));
            System.out.println("Column Type Name of 1st column: "+rsmd.getColumnTypeName(2));
            String qq="";
            while(rs.next())
            {
                flag = 1;
                for(int i=1;i<=colc;i++)
                {
                    //System.out.println("hii");
                    if((rsmd.getColumnTypeName(i)).equals("NUMBER"))
                    {
//qq += "rs.getInt('"+i+"')"+" ";
                        System.out.println(rsmd.getColumnName(i)+"   "+rs.getInt(i));
                        
                    }
                    else if(rsmd.getColumnTypeName(i).equals("VARCHAR2"))
                    {
//qq+= "rs.getString('"+i+"')"+" ";
                        System.out.println(rsmd.getColumnName(i)+"   "+rs.getString(i));
                    }
                    
                    
                }
//System.out.println(qq);
            }      
            if(flag==1)
        System.out.println("table retrieved in retrieve table method");
            else
             System.out.println("the record is not found");   
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseCrudOperations.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("table not exists please check u have passed correct table name");
            return -1;
        }
    
    
    return flag;
    }
    public static int deleteTable(String dtname,int pdt,String query,Statement st)
    {
    int i=0;
        try {
          i =   st.executeUpdate(query);
         System.out.println(i);
         if(i==1)
             System.out.println("record deleted in delete table method");
         else
         {
             
             System.out.println("record not deleted ");
         }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseCrudOperations.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("record or table not exists please check u have given correct values");
            return -1;
        }
    
    return i;
    
    }
    public static int updateTable(String utname,int put,String fut, String vut,Statement st)
    {
    int i=0;
        try {
            System.out.println(vut);
        i =    st.executeUpdate("update "+utname+" set "+fut+" = "+vut+" where id="+put);
         if(i==1)
             System.out.println("record updated in updated table method");
         else
         {
             
             System.out.println("record not updated ");
         }
            // System.out.println("record updated in update table method");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseCrudOperations.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("record or table not exists please check u have given correct values");
            return -1;
        }
    
    return i;
    }
    public static void main(String args[])
     {
     
     
     
     
     }
}
